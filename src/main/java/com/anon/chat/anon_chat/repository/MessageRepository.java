package com.anon.chat.anon_chat.repository;

import com.anon.chat.anon_chat.chat.Chat;
import com.anon.chat.anon_chat.chat.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {
    LinkedList<Message> getAllByChat(Chat chat);

    LinkedList<Message> getAllByChatOrderByTimestamp(Chat chat);
}
