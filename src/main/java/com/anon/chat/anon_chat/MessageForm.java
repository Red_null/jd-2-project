package com.anon.chat.anon_chat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MessageForm {
    @NotNull
    @Size(min = 1)
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
