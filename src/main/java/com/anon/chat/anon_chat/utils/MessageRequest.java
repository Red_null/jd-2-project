package com.anon.chat.anon_chat.utils;

import com.anon.chat.anon_chat.chat.ChatMember;
import com.anon.chat.anon_chat.chat.CommonChatMember;

public class MessageRequest {
    private CommonChatMember user;
    private String message;

    public ChatMember getUser() {
        return user;
    }

    public void setUser(CommonChatMember user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
