package com.anon.chat.anon_chat.utils;

import com.anon.chat.anon_chat.chat.Chat;
import com.anon.chat.anon_chat.chat.Message;

import java.util.LinkedList;

public class ChatResponse {
    private Chat chat;
    private int pages;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public ChatResponse(Chat chat, LinkedList<Message> messages, int pages) {
        this.chat = chat;
        this.messages = messages;
        this.pages = pages;
    }

    private LinkedList<Message> messages;

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public LinkedList<Message> getMessages() {
        return messages;
    }

    public void setMessages(LinkedList<Message> messages) {
        this.messages = messages;
    }
}
