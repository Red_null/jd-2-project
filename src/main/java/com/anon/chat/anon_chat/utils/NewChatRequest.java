package com.anon.chat.anon_chat.utils;

public class NewChatRequest {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
