package com.anon.chat.anon_chat;

import com.anon.chat.anon_chat.chat.*;
import com.anon.chat.anon_chat.utils.ChatResponse;
import com.anon.chat.anon_chat.utils.HibernateProxyTypeAdapter;
import com.anon.chat.anon_chat.utils.MessageRequest;
import com.anon.chat.anon_chat.utils.NewChatRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.LinkedList;

enum SortOrder {asc, dsc}


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ApiChatController {
    private static final Logger log = LogManager.getLogger(Class.class.getName());

    @Autowired
    ChatController chatController;


    @GetMapping("")
    public String getUserId() {
        ChatMember user = new CommonChatMember();
        return user.getId();
    }

    @GetMapping("/chat")
    public String chat() {
        Chat chat = chatController.getRandomChat();
        log.info("Redirect to chat: #" + chat.getId());
        return chat.getId();
    }

    @PostMapping("/chat/{chatId}")
    public String enterChat(@PathVariable int chatId, @RequestBody CommonChatMember user,
                            @RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "10") int size,
                            @RequestParam(value = "sort", defaultValue = "asc") SortOrder sorting) {
        Chat chat;
        Gson gson = getGson();
        try {
            chat = chatController.getChat(chatId);
            chatController.enterChat(chat, user.getId());
            LinkedList<Message> messages = chatController.getChatMessages(chat);
            if (sorting == SortOrder.dsc) {
                Collections.reverse(messages);
            }

            LinkedList<Message> resp;
            int startIdx = (page - 1) * size;

            if (startIdx > messages.size()) {
                resp = new LinkedList<>();
            } else {
                int endIdx = startIdx + size > messages.size() ? messages.size() : startIdx + size;
                resp = new LinkedList<>(messages.subList(startIdx, endIdx));
            }

            ChatResponse response = new ChatResponse(chat, resp, (int) Math.ceil((float) messages.size() / size));
            return gson.toJson(response);
        } catch (IndexOutOfBoundsException e) {
            log.warn("No such chat: #" + chatId);
        }

        return "Nope";
    }

    @PostMapping("/chat/{chatId}/send")
    public ResponseEntity<String> sendMessage(@PathVariable int chatId,
                                              @RequestBody MessageRequest request) {
        Chat chat = chatController.getChat(chatId);
        if (chatController.sendMessage(chat, request.getUser(), request.getMessage())) {
            log.warn("OPAAAAAA");
            return new ResponseEntity<>("ok", HttpStatus.OK);
        }
        return new ResponseEntity<>("not ok", HttpStatus.I_AM_A_TEAPOT);
    }

    @PostMapping("/chat/{chatId}/edit/{messageId}")
    public ResponseEntity<HttpStatus> editMessage(@PathVariable int chatId, @PathVariable long messageId,
                                                  @RequestBody MessageRequest request) {
        if (chatController.editMessage(messageId, request.getMessage())) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT);

    }

    @DeleteMapping("/chat/{chatId}/delete/{messageId}")
    public ResponseEntity<HttpStatus> deleteMessage(
            @PathVariable int chatId,
            @PathVariable long messageId) {
        if (chatController.deleteMessage(messageId)) {
            return new ResponseEntity<>(HttpStatus.OK);

        }
        return new ResponseEntity<>(HttpStatus.I_AM_A_TEAPOT);
    }

    @PostMapping("/chat/new")
    public String createChat(@RequestBody NewChatRequest request) {
        Chat chat = chatController.createChat(request.getTitle());
        return chat.getId();
    }

    private Gson getGson() {
        GsonBuilder b = new GsonBuilder();
        b.registerTypeAdapterFactory(HibernateProxyTypeAdapter.FACTORY);
        return b.create();
    }
}
