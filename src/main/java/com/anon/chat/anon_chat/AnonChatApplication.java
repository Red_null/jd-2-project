package com.anon.chat.anon_chat;

import com.anon.chat.anon_chat.auth.UserRepository;
import com.anon.chat.anon_chat.repository.ChatRepository;
import com.anon.chat.anon_chat.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnonChatApplication {
    @Autowired
    MessageRepository messageRepository;

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(AnonChatApplication.class, args);
    }

}

