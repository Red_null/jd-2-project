package com.anon.chat.anon_chat.chat;

import java.util.*;

public class CommonChatMember implements ChatMember {
    private static int users = 0;
    private String id;

    public CommonChatMember() {
        this.id = UUID.randomUUID().toString();
    }

    public CommonChatMember(String userId) {
        this.id = userId;
    }


    @Override
    public String toString() {
        return "CommonChatMember{" +
                "id='#" + id + '\'' +
                '}';
    }

    @Override
    public String getId() {
        return this.id;
    }
}
