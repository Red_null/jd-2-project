package com.anon.chat.anon_chat.chat;

import com.anon.chat.anon_chat.repository.ChatRepository;
import com.anon.chat.anon_chat.repository.MessageRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.LinkedList;

@Controller
public class ChatController {
    private static final Logger log = LogManager.getLogger(Class.class.getName());

    @Autowired
    ChatPool pool;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    ChatRepository chatRepository;

    public Chat getChat(int chatId) {
        return pool.getChat(chatId);
    }

    public Chat getRandomChat() {
        Chat chat = this.pool.getRandomChat();
        log.info("Redirect to chat: #" + chat.getId());
        return chat;
    }

    public Chat createChat(String name) {
        Chat chat = new Chat(name);
        this.pool.addChat(chat);
        return chat;
    }

    public void enterChat(Chat chat, String userId) {
        ChatMember user = new CommonChatMember(userId);
        chat.addMember(user);
        log.info("Connect to chat: #" + chat.getId());
    }

    public LinkedList<Message> getChatMessages(Chat chat) {
        return messageRepository.getAllByChatOrderByTimestamp(chat);
    }

    public boolean sendMessage(Chat chat, ChatMember user, String text) {
        Message message = new Message(text, user.getId());
//        log.info("Send message to #" + chat.getId() + " : " + message);
        try {
            message.setChat(chat);
            messageRepository.save(message);
        } catch (Exception e) {
            log.error("Cant send message :" + message);
            return false;
        }
        return true;
    }

    public boolean editMessage(long messageId, String text) {
        if (messageRepository.findById(messageId).isPresent()) {
            log.info("Edit msg: " + messageId + " : " + text);
            Message message = messageRepository.findById(messageId).get();
            message.setText(text);
            messageRepository.save(message);
        } else {
            log.info("No such msg: " + messageId);
            return false;
        }
        return true;
    }

    public boolean deleteMessage(long messageId) {
        if (messageRepository.findById(messageId).isPresent()) {
            Message message = messageRepository.findById(messageId).get();
            messageRepository.delete(message);
            return true;
        }
        return false;
    }
}
