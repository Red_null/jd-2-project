package com.anon.chat.anon_chat.chat;

import com.anon.chat.anon_chat.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class ChatPool {
    @Autowired
    ChatRepository chatRepository;

    private Map<Long, Chat> pool;

    public ChatPool() {
        this.pool = new LinkedHashMap<>();
    }

    @PostConstruct
    public void init() {
        if (this.chatRepository.count() > 0) {
            for (Chat chat : (this.chatRepository.findAll())) {
                this.pool.put(chat.getChatId(), chat);
            }
        } else {
            this.addChat(new Chat());
        }
    }

    public Chat getRandomChat() throws IndexOutOfBoundsException {
        Object idx = pool.keySet().toArray()[new Random().nextInt(pool.keySet().toArray().length)];
        return pool.get(idx);

    }

    public Chat addChat(Chat chat) {
        chatRepository.save(chat);
        this.pool.put(chat.getChatId(), chat);
        return chat;
    }

    public Chat getChat(int id) {
        return this.pool.get(Integer.toUnsignedLong(id));
    }
}
