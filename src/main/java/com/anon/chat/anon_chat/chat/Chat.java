package com.anon.chat.anon_chat.chat;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Chat {
    @Transient
    private List<ChatMember> members;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long chatId;

    private String title;


    public Chat() {
        super();
        this.members = new LinkedList<>();
        this.title = "Chat #";
    }

    public Chat(String title) {
        super();
        this.members = new LinkedList<>();
        this.title = title;
    }

    public void addMember(ChatMember newMember) {
        for (ChatMember member : this.members) {
            if (member.getId().equals(newMember.getId())) {
                return;
            }
        }
        this.members.add(newMember);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void removeMember(ChatMember member) {
        this.members.remove(member);
    }


    public String getId() {
        return Long.toString(this.chatId);
    }

    public List<ChatMember> getMembers() {
        return members;
    }


    public Long getChatId() {
        return chatId;
    }
}

