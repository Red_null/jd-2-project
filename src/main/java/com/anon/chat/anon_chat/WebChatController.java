package com.anon.chat.anon_chat;

import com.anon.chat.anon_chat.chat.*;
import com.anon.chat.anon_chat.utils.SecurityHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.LinkedList;

@Controller
public class WebChatController {
    private static final Logger log = LogManager.getLogger(Class.class.getName());
    private final int COOKIE_TIMEOUT = 5 * 60;

    @Autowired
    ChatController chatController;

    @GetMapping("/")
    public String home(Model model,
                       HttpServletResponse response,
                       @CookieValue(value = "userId", required = false) String userId) {
        ChatMember user = createOrGetUser(userId, response);
        model.addAttribute("user", user);
        model.addAttribute("isAdmin", SecurityHelper.isAdmin());

        this.updateUserCookie(response, user);
        return "index";
    }

    @GetMapping("/i")
    public String api() {
        return "client";
    }

    @GetMapping("/chat")
    public RedirectView chat(HttpServletResponse response,
                             @CookieValue(value = "userId", required = false) String userId) {

        ChatMember user = createOrGetUser(userId, response);
        this.updateUserCookie(response, user);

        Chat chat = chatController.getRandomChat();
        chat.addMember(user);

        log.info("Redirect to chat: #" + chat.getId());
        return new RedirectView("/chat/" + chat.getId());
    }

    @RequestMapping("/chat/{chatId}")
    public ModelAndView enterChat(ModelMap model,
                                  @PathVariable int chatId,
                                  HttpServletResponse response,
                                  @CookieValue(value = "userId") String userId) {
        Chat chat;
        try {
            chat = chatController.getChat(chatId);
        } catch (IndexOutOfBoundsException e) {
            log.warn("No such chat: #" + chatId);
            model.addAttribute(userId);
            return new ModelAndView("forward:/chat", model);
        }

        ChatMember user = new CommonChatMember(userId);
        this.updateUserCookie(response, user);

        chatController.enterChat(chat, userId);

        MessageForm messageForm = new MessageForm();
        LinkedList<Message> messages = chatController.getChatMessages(chat);

        model.addAttribute(chat);
        model.addAttribute("userId", user.getId());
        model.addAttribute(messageForm);
        model.addAttribute("messages", messages);
        model.addAttribute("isAdmin", SecurityHelper.isAdmin());
        return new ModelAndView("chat", model);
    }

    @PostMapping("/chat/send/{chatId}")
    public RedirectView sendMessage(Model model,
                                    @PathVariable int chatId,
                                    HttpServletResponse response,
                                    @CookieValue(value = "userId") String userId,
                                    @Valid MessageForm messageForm, BindingResult bindingResult) {


        if (bindingResult.hasErrors()) {
            return new RedirectView("/");
        }

        ChatMember user = new CommonChatMember(userId);
        Chat chat = chatController.getChat(chatId);
        chatController.sendMessage(chat, user, messageForm.getMessage());

        this.updateUserCookie(response, user);
        return new RedirectView("/chat/" + chatId);

    }

    @PostMapping("/chat/{chatId}/edit/{messageId}")
    public RedirectView editMessage(@PathVariable int chatId, @PathVariable long messageId,
                                    @Valid MessageForm messageForm, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return new RedirectView("/");
        }
        chatController.editMessage(messageId, messageForm.getMessage());
        return new RedirectView("/chat/" + chatId);
    }

    @GetMapping("/chat/{chatId}/delete/{messageId}")
    public RedirectView deleteMessage(
            @PathVariable int chatId,
            @PathVariable long messageId) {
        chatController.deleteMessage(messageId);
        return new RedirectView("/chat/" + chatId);

    }

    @RequestMapping("/new_user")
    public String new_user(Model model, HttpServletResponse response) {
        ChatMember user = this.createNewUser(response);
        return home(model, response, user.getId());
    }

    private ChatMember createNewUser(HttpServletResponse response) {
        ChatMember user = new CommonChatMember();
        this.removeUserCookie(response);
        this.updateUserCookie(response, user);
        return user;
    }

    private ChatMember createOrGetUser(String userId, HttpServletResponse response) {
        return userId == null ? createNewUser(response) : new CommonChatMember(userId);
    }

    private void updateUserCookie(HttpServletResponse response, ChatMember user) {
        Cookie cookie = new Cookie("userId", user.getId());
        cookie.setMaxAge(this.COOKIE_TIMEOUT);
        response.addCookie(cookie);
    }

    private void removeUserCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie("userId", null);
        cookie.setPath("/chat");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }
}
